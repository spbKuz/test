package com.example.testapplication;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends ActionBarActivity implements OnTouchListener  {

    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        globalN = 5;
        globalM = 3;

        color_array = new int [] {
                Color.RED,
                Color.BLUE,
                Color.GREEN,
                Color.YELLOW,
                Color.BLACK
        };

        takeElement=false;
        //"координты" нажимаемых элементов
        elementStart=new int[2];
        elementFinish=new int[2];

        LinearLayout ll_vert = new TableLayout(this);
        ll_vert.setOrientation(LinearLayout.VERTICAL);
        ll_vert.setGravity(Gravity.CENTER);

        //определение размера фишек
        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics metricsB = new DisplayMetrics();
        display.getMetrics(metricsB);

        moveList = new MoveList();
        listElementMove = new MoveList();
        stat = new Statistic();

        if(isData()) {
            prefs = getSharedPreferences(PREFS, MODE_PRIVATE);

            N = prefs.getInt("N", NOTHING_SELECTED);
            M = prefs.getInt("M", NOTHING_SELECTED);
            size = prefs.getInt("size", NOTHING_SELECTED);
        }
        else {
            N = globalN;
            M = globalM;
            size = 20;
        }

        element_array = new Element[N][N];

        //инициализациия элементов массива
        for (int i = 0; i < N; i++) {
            LinearLayout ll_hor = new LinearLayout(this);
            ll_hor.setOrientation(LinearLayout.HORIZONTAL);
            ll_hor.setGravity(Gravity.CENTER);

            for (int j = 0; j < N; j++) {
                 //создание фишечки
                 ImageView image = new ImageView(this);

                 //подключаем обрабочик
                 image.setOnTouchListener(this);

                  //создаём элемент фишечки
                  element_array[i][j] = new Element(image);

                  if(isData())
                     repaintElement(element_array[i][j], prefs.getInt("" + i + "," + j,NOTHING_SELECTED));

                  //добавляем в строку
                  ll_hor.addView(element_array[i][j].image);
            }
             ll_vert.addView(ll_hor);
        }
        //подключчаем компоновку
        setContentView( ll_vert);

        if(!isData()) {
            //заполнение поля
            initialize();
            //очищаем и добавляем новые лементы пока это возможно
            findLine();
        }
    }

    //==============
    //поиск возможны ходов
    public void isMove(){
        moveList.clear();

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                //поиск ситуаций первого типа
                isFigure1(i, j, i - 1, j);
                isFigure1(i, j, i + 1, j);
                isFigure1(i, j, i, j - 1);
                isFigure1(i, j, i, j + 1);

                //поиск ситуаций 2-ого типа
                isFigure2(i, j, i - 2, j);
                isFigure2(i, j, i + 2, j);
                isFigure2(i, j, i, j - 2);
                isFigure2(i, j, i, j + 2);
            }
        }
    }

    //поиск ситуаций первого типа
    public void isFigure1(int i1,int j1,int i2,int j2){
        int i3, j3,i4,j4;
        int colorTemp;

        if (isElement(i1, j1)) {
            colorTemp = element_array[i1][j1].color;
            if (isElementColor(i2, j2, colorTemp)){
                if(i1==i2){
                    if(j1>j2){
                        j3=j1+1;
                        j4=j1-2;

                        //частные случани
                        if (isElementColor(i1,j3+1 , colorTemp)){
                            moveList.add(i1,j3,i1,j3+1);
                        }
                        if (isElementColor(i1,j4-1 , colorTemp)){
                            moveList.add(i1,j4,i1,j4-1);
                        }

                    }else{
                        j3=j1-1;
                        j4=j1+2;

                        if (isElementColor(i1,j3-1 , colorTemp)){
                            moveList.add(i1,j3,i1,j3-1);
                        }
                        if (isElementColor(i1,j4+1 , colorTemp)){
                            moveList.add(i1,j4,i1,j4+1);
                        }
                    }

                    //общие случани
                    if (isElementColor(i1+1,j3, colorTemp)){
                        moveList.add(i1,j3,i1+1,j3);
                    }
                    if (isElementColor(i1-1,j4, colorTemp)){
                        moveList.add(i1,j4,i1-1,j4);
                    }

                }else{
                    if(i1>i2){
                        i3=i1+1;
                        i4=i1-2;

                        //частные случани
                        if (isElementColor(i3+1,j1 , colorTemp)){
                            moveList.add(i3,j1,i3+1,j1);
                        }
                        if (isElementColor(i4-1,j1 , colorTemp)){
                            moveList.add(i4,j1,i4-1,j1);
                        }

                    }else{
                        i3=i1-1;
                        i4=i1+2;

                        if (isElementColor(i3-1,j1 , colorTemp)){
                            moveList.add(i3,j1,i3-1,j1);
                        }
                        if (isElementColor(i4+1,j1 , colorTemp)){
                            moveList.add(i4,j1,i4+1,j1);
                        }
                    }

                    //общие случани
                    if (isElementColor(i3,j1+1, colorTemp)){
                        moveList.add(i3,j1,i3,j1+1);

                    }
                    if (isElementColor(i4,j1-1, colorTemp)){
                        moveList.add(i4,j1,i4,j1-1);
                    }
                }
            }
        }
    }

    //поиск ситуаций первого типа
    public void isFigure2(int i1,int j1,int i2,int j2){
        int i3, j3;
        int colorTemp;

        if (isElement(i1, j1)) {
            colorTemp = element_array[i1][j1].color;
            if (isElementColor(i2, j2, colorTemp)){
                if(i1==i2) {  //на одной линии поверикали
                    //координаты фишки между двумя другими
                    i3=i1;

                    if(j1>j2)
                        j3=j1-1;
                    else
                        j3=j1+1;

                    //проверяем точки левее и правее серидины
                    if (isElementColor(i3-1, j3, colorTemp)){
                        //добовляется 2 точки, одну которую нашли, другая соотвествует тому месту куда должна переместится фишка
                        moveList.add(i3-1,j3,i3,j3);
                    }
                    if (isElementColor(i3+1, j3, colorTemp)){
                        moveList.add(i3+1,j3,i3,j3);
                    }
                } else{
                    if(i1>i2)
                        i3=i1-1;
                    else
                        i3=i1+1;

                    j3=j1;
                    //проверяем точки выше и ниже серидины
                    if (isElementColor(i3, j3-1, colorTemp)){
                        //добовляется 2 точки, одну которую нашли, другая соотвествует тому месту куда должна переместится фишка
                        moveList.add(i3,j3-1,i3,j3);
                    }
                    if (isElementColor(i3, j3+1, colorTemp)){
                        moveList.add(i3,j3+1,i3,j3);
                    }
                }
            }
        }
    }

    //===================
    //чистим поле
    public void clear(){
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                element_array[i][j].delete();
    }

    //заполнениt фигур
    public boolean figure1(int i1,int j1,int i2,int j2, int color){
        int i3,j3,i4,j4,i5,j5;

        //оба элемента доступны
        if (isElementNun(i1, j1)) {
            if (isElementNun(i2, j2)) {
                //доп элемент чтобы можно было сделать ход
                if (isElementNun(i3=i1+(i1-i2)+(j1-j2),j3=j1+(j1-j2)+(i1-i2))) {
                    repaintElement(element_array[i3][j3], color);
                } else if(isElementNun(i3=i1+(i1-i2)-(j1-j2),j3=j1+(j1-j2)-(i1-i2))) {
                    repaintElement(element_array[i3][j3], color);
                } else if (isElementNun(i3=i1-2*(i1-i2)+(j1-j2),j3=j1-2*(j1-j2)+(i1-i2))) {
                    repaintElement(element_array[i3][j3], color);
                } else if (isElementNun(i3=i1-2*(i1-i2)-(j1-j2),j3=j1-2*(j1-j2)-(i1-i2))) {
                    repaintElement(element_array[i3][j3], color);
                } else
                    return false;
            }
            else
                return false;
        }
        else
            return false;

        repaintElement(element_array[i1][j1], color);
        repaintElement(element_array[i2][j2], color);

        int tempColor;
        if(color==0){
            tempColor=color+1;
        }
        else{
            tempColor=color-1;
        }

        if (isElementNun(i4=i1+(i1-i2),j4=j1+(j1-j2))) {
            repaintElement(element_array[i4][j4], tempColor);
        }

        if (isElementNun(i5=i1-2*(i1-i2),j5=j1-2*(j1-j2))) {
            repaintElement(element_array[i5][j5], tempColor);
        }

        return true;
    }

    //заполнение (перезаполнение) поля
    public void initialize(){
        clear();
        //колличество цветов используемых как "основные"
        int norm=Math.min(M,N-3);

        Random random = new Random();
        int i1,j1;

        for(int color=0;color<norm;color++){
            while (true) {
                i1 = random.nextInt(N);
                j1 = random.nextInt(N);

                if (figure1(i1, j1, i1 - 1, j1, color)) { break; }
                else if (figure1(i1, j1, i1 + 1, j1, color)) { break;  }
                else if (figure1(i1, j1, i1, j1 - 1, color)) { break;  }
                else if (figure1(i1, j1, i1, j1 + 1, color)) { break;  }
            }
        }
        //остальное заполняется рандомно
        filling();
    }

    //заполнение пустых эементов рандомными фишками
    public void filling(){
        Random random = new Random();
        //остальное заполняется рандомно
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (element_array[i][j].color == -1) {
                    repaintElement(element_array[i][j], random.nextInt(M));
                }
            }
        }
    }

    //===================
    //пустой доступный (не выходит за пределы массива) элемент
    public boolean isElementNun(int i,int j){
        if(i>=0 && i<N && j>=0 && j<N)
            if(element_array[i][j].color==-1 )
                return true;
        return false;
    }

    //доступный (не выходит за пределы массива) не пустой элемент
    public boolean isElement(int i,int j){
        if(i>=0 && i<N && j>=0 && j<N)
            if(element_array[i][j].color!=-1 )
                return true;
        return false;
    }

    //доступный (не выходит за пределы массива) элемент с одинаковым color
    public boolean isElementColor(int i,int j, int color){
        if(i>=0 && i<N && j>=0 && j<N)
            if(element_array[i][j].color==color )
                return true;
        return false;
    }

    //поиск "координат" элемета в массиве по ImageView
    public void findElement (ImageView image, int[] elementTake){
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                if (image.equals(element_array[i][j].image)) {
                    elementTake[0] = i;
                    elementTake[1] = j;
                    return;
                }
        elementTake[0] = -1;
        elementTake[1] = -1;
    }

    //===================
    //перерисовка элемента
    public void repaintElement(Element element, int color){
        //Перерисовка началного элемента
        Bitmap temp = Bitmap.createBitmap(size, size, Bitmap.Config.RGB_565);

        //в дальнешем убрать, но сейчас все пустые элементы для наглядности отображаются белым (последним в скписке цветов)
        if(color==-1)
            temp.eraseColor(color_array[0]);
        else
            temp.eraseColor(color_array[color]);

        element.color=color;
        element.image.setImageBitmap(temp);
    }

    //ракировка элментов
    public void move (int i1,int j1, int i2, int j2){
         //Запоминаю цвета жлементов
        int tempColor = element_array[i1] [j1 ].color;

        //Перерисовка начального элемента
        repaintElement(element_array[i1][j1], element_array[i2][j2].color);
        //Перерисовка конечного элемента
        repaintElement(element_array[i2] [j2],tempColor);
    }

    //===================
    //нажатие
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getActionMasked()) {
            case MotionEvent.ACTION_DOWN:
                if (takeElement){
                    //запоминаю вторую фишечку и перемещаю
                    findElement( (ImageView) v , elementFinish);

                    if(elementFinish[0]!=-1 && elementFinish[1]!=-1) {
                        //если это не тот же элемент
                        if (elementFinish[0] != elementStart[0] || elementFinish[1] != elementStart[1]) {
                            //если туда можно двигаться то передвигаемся
                            if (listElementMove.isElement(elementFinish[0], elementFinish[1])) {
                                move(elementStart[0], elementStart[1], elementFinish[0], elementFinish[1]);
                                //очищаем и добавляем новые лементы пока это возможно
                                findLine();
                                //дабавление в статистику хода
                                stat.move++;
                                takeElement = false;
                            } else {
                                Toast.makeText(this, "Ход невозможен!", Toast.LENGTH_SHORT).show();
                            }
                        }
                        else {      //если элемент тот же то сбрасываем
                            takeElement = false;
                        }
                    }
                }
                else{
                    //запоминаю первую фишечку
                    findElement( (ImageView) v , elementStart);
                    if(elementStart[0]!=-1 && elementStart[1]!=-1){
                        reestablish();
                        //проверка возможных ходов
                        isMove();
                        if(moveList.isMove()) {
                            //очищаю список досту
                            listElementMove.clear();
                            listElementMove = moveList.whereMove(elementStart[0], elementStart[1]);

                            if (listElementMove.isMove()) {
                                takeElement = true;
                            }
                            else
                                Toast.makeText(this, "Ход невозможен!", Toast.LENGTH_SHORT).show();
                        }
                        else{   //если ход не возможен
                            //дабавление в статистику форсированной перестановки
                            stat.crach++;
                            //заново заполняю элементами
                            initialize();
                            //очищаем и добавляем новые лементы пока это возможно
                            findLine();
                        }
                    }
                }
                break;
            default:
                break;
        }
        return false;
    }

    //====================

    //смещение и добавление новых фишек на поле (после удаления)
    public void reestablish(){
        //перебираем снизу вверх
        for(int z=0;z<N;z++) {
            for (int i = N - 1; i >= 0; i--) {
                for (int j = N - 1; j >= 0; j--) {
                    if (isElementNun(i, j))
                        if (i - 1 >= 0) {
                            move(i, j, i - 1, j);
                        }
                }
            }
        }
        //пустые заполняются рандомно
        filling();
    }

    //поиск линий для удаления
    public boolean findLine(){
        boolean resault=false;
        int tempColor;

        int iStart,jStart,iStop,jStop;

        int item;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                if (isElement(i, j)) {
                    tempColor = element_array[i][j].color;
                    iStart = i;

                    item = i;
                    while (isElementColor(item, j, tempColor)) {
                            item++;
                    }
                    iStop = item;

                    if (Math.abs(iStart - iStop) > 2) {
                        removeLine(iStart, j, iStop, j);
                        resault=true;
                    } else {
                        jStart=j;

                        item = j;
                        while (isElementColor(i, item, tempColor)) {
                                item++;
                        }
                        jStop = item;

                        if (Math.abs(jStart - jStop) > 2) {
                            removeLine(i, jStart, i, jStop);
                            resault=true;
                        }
                    }
                }
            }
        }

        //если было удаление, смещаем и добавляем новые элементы, затем опять проверяем на "линии"
        if(resault) {
            reestablish();
            findLine();
        }

        return resault;
    }

    //убрать линию
    public void removeLine(int i1,int j1,int i2,int j2){
        if(i1==i2){
             for (int  z=Math.min(j1,j2); z<Math.max(j1,j2) ; z++) {
                 repaintElement(element_array[i1][z],0);
                 element_array[i1][z].delete();
             }

        }
        else{
            for (int  z=Math.min(i1,i2); z<Math.max(i1,i2) ; z++) {
                repaintElement(element_array[z][j1], 0);
                element_array[z][j1].delete();
            }
        }
        //дабавление в статистику число "съеденых" линий
        stat.line++;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();


        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy(){
        //всё что нужно сохраняю
        saveData();
        super.onDestroy();
    }

    @Override
    public void onStop(){
        //всё что нужно сохраняю
        saveData();
        super.onStop();
    }

    //сохранения и т.д.

    public void initData(){
        prefs = getSharedPreferences(PREFS, MODE_PRIVATE);

        N = prefs.getInt("N", NOTHING_SELECTED);
        M = prefs.getInt("M", NOTHING_SELECTED);
        size = prefs.getInt("size", NOTHING_SELECTED);

        element_array = new Element[N][N];

        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++) {
                element_array[i][j].color=prefs.getInt("" + i + "," + j,NOTHING_SELECTED);
            }
    }

    //есть ли сохранённые параметры
    public boolean isData(){
        prefs = getSharedPreferences(PREFS, MODE_PRIVATE);
        //если M и N не менять, то восстановление идёт нормально, но если изменить то могут возникнуть не состыковки
        //по этому вводятся глобальные M и N + дополнительная проверка
        return prefs.getInt("N", NOTHING_SELECTED)!=-1 && prefs.getInt("N", NOTHING_SELECTED)==globalN && prefs.getInt("M", NOTHING_SELECTED)==globalM;
    }

    public void saveData(){
        prefs = getSharedPreferences(PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        editor.putInt("N",N);
        editor.commit();

        editor.putInt("M",M);
        editor.commit();

        editor.putInt("size",size);
        editor.commit();

        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++) {
                editor.putInt("" + i + "," + j,element_array[i][j].color);
                editor.commit();
            }

    }
}
