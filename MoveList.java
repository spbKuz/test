package com.example.testapplication;


import java.util.ArrayList;


/**
 * Created by Администратор on 23.02.2015.
 */
public class MoveList {

    public ArrayList list=new ArrayList();

    //поиск возможных ходов фишки
    public MoveList whereMove(int i,int j){
        //доступные перемещения дл конкретной фишки
        MoveList listElementMove=new MoveList();

        //пары точек идут в связке, фишка и то куда она может пойти
        for(int z=0;z<list.size();z+=4) {
            if(list.get(z)==i && list.get(z+1)==j ){
                listElementMove.add( (int) list.get(z+2));
                listElementMove.add(  (int) list.get(z+3));

            }
            else if(list.get(z+2)==i && list.get(z+3)==j ){
                listElementMove.add( (int) list.get(z));
                listElementMove.add( (int) list.get(z+1));
            }
        }
        return listElementMove;
    }

    public void clear(){
        list.clear();
    }

    //существуют ли ходы, или надо поле перезаливать
    public boolean isMove(){
        return list.size()>0;
    }
}
